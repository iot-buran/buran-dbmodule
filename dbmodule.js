let MongoClient = require('mongodb').MongoClient;
let ObjectID = require('mongodb').ObjectID

const DB_URL = process.env.DB_URL || "mongodb://localhost:27017";

const dbName = 'buran-db';
const deviceTable = "devices"
const targetGroupsTable = "targetgroups"
const campaignsTable = "campaigns"
const usersTable = "users"

// Create a new MongoClient
const client = new MongoClient(DB_URL, {useNewUrlParser: true});

let db;
// Use connect method to connect to the Server

let connectToDB = async errorCallback => {
    return client.connect()
      .then(client => { 
        console.log(`Connected successfully to database server on url ${DB_URL}`);
      
        db = client.db(dbName);
        //createDemoDevices()

      })
      .catch(err => {
        console.log(`Cannot connect to mongo server: ${err}`)
        if (errorCallback) {
          errorCallback(err)
        }
      })
}

let closeDB = () => {
    client.close()
}

let insertNewTargetGroup = targetGroup => {
  return db.collection(targetGroupsTable).insertOne(targetGroup)
}

let getAllTargetGroupsForUser = async (userId) => {
  return db.collection(targetGroupsTable).find({user: userId}).toArray()
}

let getTargetGroupById = async targetGroupId => {
  return db.collection(targetGroupsTable).findOne({_id: ObjectID(targetGroupId)})
}

let deteleTargetGroup = async targetGroupId => {
  return db.collection(targetGroupsTable).deleteOne({_id: ObjectID(targetGroupId)})
}

let upsertDevice = (deviceId, fields) => {
    return db.collection(deviceTable).updateOne({_id:deviceId}, {$set: fields}, {upsert:true})
}

let getAllDevicesForUser = async userId => {
    return db.collection(deviceTable).find({user: userId}).toArray()
}

let getFilteredDevicesForUser = async (userId, conditions) => {
   conditions.user = userId
   return db.collection(deviceTable).find(conditions).toArray()
}

let getDeviceById = async deviceId => {
  return db.collection(deviceTable).findOne({_id: deviceId})
}

let deteleDevice = async deviceId => {
  return db.collection(deviceTable).deleteOne({_id: deviceId})
}

let insertNewCampaign = campaign => {
  return db.collection(campaignsTable).insertOne(campaign)
}

let getAllCampaignsForUser = async (userId) => {
  return db.collection(campaignsTable).find({user: userId}).toArray()
}

let getFilteredCampaigns = async filter => {
  return db.collection(campaignsTable).find(filter).toArray()
}

let deteleCampaign = async campaignId => {
  return db.collection(campaignsTable).deleteOne({_id: ObjectID(campaignId)})
}

let getCampaignById = async campaignId => {
  return db.collection(campaignsTable).findOne({_id: ObjectID(campaignId)})
}

let upsertCampaign = async (campaignId, fields) => {
  return db.collection(campaignsTable).updateOne({_id:ObjectID(campaignId)}, {$set: fields}, {upsert:true})
}


let upsertCampaignDeviceStatus = async (campaignId, deviceId, status) => {
  return db.collection(campaignsTable).updateOne({_id:ObjectID(campaignId), "devices.id":deviceId}, {$set: {"devices.$.status":status}}, {upsert:true})
}

let insertNewUser = user => {
  return db.collection(usersTable).insertOne(user)
}

let upsertUser = user => {
  return db.collection(usersTable).updateOne({_id:user._id}, {$set: user}, {upsert:true})
}

let getUserByEmail = async email => {
  return db.collection(usersTable).findOne({email: email})
}

let getUserByEmailAndPass = async (email, pass) => {
  return db.collection(usersTable).findOne({email: email, encodedPassword: pass})
}

let createDemoDevices = () => {
  let devices = [
    {_id: "12353423", type: "NodeMCU", firmwareVersion: "0.0.5", group: "Home", stat: "Online", lastOnline: "now"},
    {_id: "78566756", type: "NodeMCU", firmwareVersion: "1.0.5", group: "Work", stat: "Offline", lastOnline: "25/04/2019 18:05:52", user: "54321"},
    {_id: "547io4u5", type: "Esp8286", firmwareVersion: "0.2.5", group: "Garden", stat: "Online", lastOnline: "now", user: "123"},
    {_id: "45769879", type: "Olimex12", firmwareVersion: "3.0.5", group: "Home", stat: "Online", lastOnline: "now"}
  ]
  db.collection(deviceTable).drop()
  db.collection(deviceTable).insertMany(devices)
}

module.exports = {
    connectToDB,
    closeDB,
    getAllDevicesForUser,
    getFilteredDevicesForUser,
    upsertDevice,
    getDeviceById,
    insertNewTargetGroup,
    getAllTargetGroupsForUser,
    getTargetGroupById,
    deteleDevice,
    deteleTargetGroup,
    getAllCampaignsForUser,
    getFilteredCampaigns,
    getCampaignById,
    insertNewCampaign,
    upsertCampaign,
    upsertCampaignDeviceStatus,
    deteleCampaign,
    insertNewUser,
    upsertUser,
    getUserByEmail,
    getUserByEmailAndPass
}